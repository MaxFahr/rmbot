use rand::prelude::*;
extern crate dotenv;
extern crate futures;
extern crate json;
extern crate rand;
extern crate reqwest;

use futures::executor::block_on;
use reqwest::header::USER_AGENT;

pub async fn get_reddit_meme(
    client: &reqwest::Client,
) -> Result<String, Box<dyn std::error::Error>> {
    let reddit_response = client
        .get("https://www.reddit.com/r/me_irl/search.json?q=me_irl&limit=100")
        .send()
        .await?
        .text()
        .await?;

    let parsed_reddit_response = json::parse(&reddit_response).unwrap();
    let meme_number: usize = rand::thread_rng().gen_range(0, 99);
    let url = parsed_reddit_response["data"]["children"][meme_number]["data"]["url"].to_string();
    Ok(url)
}

pub async fn get_wiki_page(client: &reqwest::Client) -> Result<String, Box<dyn std::error::Error>> {
    let random_wiki_page_response = client
        .get("https://de.wikipedia.org/api/rest_v1/page/random/summary")
        .header(USER_AGENT, "maxf1990@hotmail.com")
        .send()
        .await?
        .text()
        .await?;

    let parsed_wiki_page = json::parse(&random_wiki_page_response).unwrap();
    let url = parsed_wiki_page["content_urls"]["desktop"]["page"].to_string();
    Ok(url)
}

pub async fn get_hn_story(client: &reqwest::Client) -> Result<String, Box<dyn std::error::Error>> {
    let mut url = String::from("null");
    while url == "null" {
        let all_stories = client
            .get("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
            .send()
            .await?
            .text()
            .await?;

        let parsed_stories = json::parse(&all_stories).unwrap();
        let rand: usize = rand::thread_rng().gen_range(0, 499);

        let single_story_url = format!(
            "https://hacker-news.firebaseio.com/v0/item/{id}.json?print=pretty",
            id = parsed_stories[rand]
        );
        let single_story = client.get(&single_story_url).send().await?.text().await?;

        let parsed_single_story = json::parse(&single_story).unwrap();
        url = parsed_single_story["url"].to_string();
    }
    Ok(url)
}
