use std::collections::HashMap;

extern crate reqwest;

pub struct TelegramClient {
    token: String,
    group_id: String,
}

impl TelegramClient {
    pub fn new(token: String, group_id: String) -> TelegramClient {
        TelegramClient { token, group_id }
    }

    pub async fn send_message(
        &self,
        message: String,
        client: reqwest::Client,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let url = format!("https://api.telegram.org/bot{}/sendMessage", self.token);
        let mut message_body = HashMap::new();
        message_body.insert("chat_id", &self.group_id);
        message_body.insert("text", &message);
        let message_response = client
            .get(&url)
            .json(&message_body)
            .send()
            .await?
            .text()
            .await?;

        let parsed_message_response = json::parse(&message_response).unwrap();
        println!("{:#?}", message_response);
        Ok(())
    }
}
