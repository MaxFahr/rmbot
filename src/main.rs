use rand::prelude::*;
extern crate dotenv;
extern crate futures;
extern crate json;
extern crate rand;
extern crate reqwest;

use crate::telegram_client::TelegramClient;
use futures::executor::block_on;
use futures::StreamExt;
use reqwest::header::USER_AGENT;
use telegram_bot::Message;
use telegram_bot::*;

mod sources;
mod telegram_client;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let client = reqwest::Client::new();
    let processed = process_message(client);
    match processed {
        Ok(()) => {}
        Err(error) => panic!("Problem sending message: {:?}", error),
    }
    Ok(())
}

fn choose_message_source() -> &'static str {
    let sources: [&str; 3] = ["wiki", "reddit", "hackernews"];
    let rand: usize = rand::thread_rng().gen_range(0, 3);
    sources[rand]
}

fn send_message(message: String, client: reqwest::Client) -> Result<(), Error> {
    let token = dotenv::var("TELEGRAM_BOT_TOKEN").expect("TELEGRAM_BOT_TOKEN not set");
    let chat_id =
        dotenv::var("TRIGGEROVERFLOW_GROUP_ID").expect("TRIGGEROVERFLOW_GROUP_ID not set");
    let telegram = telegram_client::TelegramClient::new(token, chat_id);
    let message_sent_future = telegram.send_message(message, client);
    block_on(message_sent_future);
    Ok(())
}

fn process_message(client: reqwest::Client) -> Result<(), Error> {
    let mut message: String;
    let message_source = choose_message_source();
    match message_source {
        "wiki" => {
            let wiki_message_future = sources::get_wiki_page(&client);
            let message_to_send = block_on(wiki_message_future);
            match message_to_send {
                Ok(msg) => {
                    message = msg;
                }
                Err(error) => panic!("Problem getting wiki page: {:?}", error),
            };
        }
        "reddit" => {
            let reddit_message_future = sources::get_reddit_meme(&client);
            let message_to_send = block_on(reddit_message_future);
            match message_to_send {
                Ok(msg) => {
                    message = msg;
                }
                Err(error) => panic!("Problem getting reddit meme: {:?}", error),
            };
        }
        "hackernews" => {
            let hn_message_future = sources::get_hn_story(&client);
            let message_to_send = block_on(hn_message_future);
            match message_to_send {
                Ok(msg) => {
                    message = msg;
                }
                Err(error) => panic!("Problem getting hn article: {:?}", error),
            };
        }
        _ => message = String::from("Der Bot hat ein Problem, bitte hilf ihm."),
    };
    let message_sent = send_message(message, client);
    match message_sent {
        Ok(()) => {}
        Err(error) => {
            println!("Problem sending message: {}", error);
        }
    }
    Ok(())
}
